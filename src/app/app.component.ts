import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  counter: number = 0;
  updateAt?: number;

  get disableDecrease(): boolean {
    return this.counter <= 0;
  }

  updateTime() {
    this.updateAt = Date.now();
  }

  increase(): void {
    this.updateTime();
    this.counter++;
  }

  decrease(): void {
    this.updateTime();
    this.counter--;
  }

  clear(): void {
    this.updateTime();
    this.counter = 0;
  }
}
